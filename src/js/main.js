const tittle = "Área do médico";
const baseUrl = "http://localhost:3000";
const loginPage = "login.html"


verifyAuth();

function getTitulo() {
    document.write("Bem vindo, " + localStorage.username);
}

function showMsg(msg, div, error, up) {
    $(div).html(msg);
    $(div).show();
    setTimeout(function() {
        $(div).hide('fade')
    }, 2000);
    if (up) document.body.scrollTop = document.documentElement.scrollTop = 0;
    if (error) throw msg;
}

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999;';  
}

function verifyAuth() {
    //setCookie('ppkcookie','testcookie',7);
    var x = getCookie('auth');
    if ((!x) && (window.location.href.indexOf(loginPage) == -1)) {
        window.location.href = loginPage;
    }
}

function apiCall(request) {
    var cback = request.callback;
    $.ajax({
            url: baseUrl + request.url,
            type: request.method,
            data: request.obj,
            beforeSend: function() {}
        })
        .done(function(data, textStatus, jqXHR) {
            cback(data, textStatus);
        })
        .fail(function(jqXHR, textStatus, msg) {
            alert(msg);
        });
}

function logout()
{
    eraseCookie('auth');
    localStorage.clear();
    window.location.href = loginPage;
}