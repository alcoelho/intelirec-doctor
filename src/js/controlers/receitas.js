medicamentoAutoComplete("#medicamento");
var receitas = [];

function getMedicamento() {
    event.preventDefault();
    $("#tab_receitas tbody").html("");
    //rescue filters
    var medicamento = $("#medicamento").val();
    var classe = $("#classe").val();
    var paciente_cpf = $("#paciente_cpf").val();
    var data_inicio = $("#data_inicio").val();
    var data_fim = $("#data_fim").val();
    if (data_inicio > data_fim) {

        var msg = "Data inicial não pode ser maior que data final";
        showMsg(msg,"#msg_error",true,true);
    }
    console.log(medicamento);
    console.log(classe);
    console.log(paciente_cpf);
    console.log(data_inicio);
    console.log(data_fim);
    $.getJSON("js/mock/receitas.json", function(data) {
        $.each(data, function(key, val) {
            $.each(val, function(k, receita) {
                var items = [];
                receitas[receita.id] = receita;
                items.push("<td><span>" + receita.id + "</span></td>");
                items.push("<td><span>" + receita.paciente + "</span></td>");
                items.push("<td><span>" + receita.medicamento + "</span></td>");
                items.push("<td><span>" + receita.classe + "</span></td>");
                items.push("<td><span>" + receita.data + "</span></td>");
                items.push("<td><a href=\"javascript:showReceitaDetail('" + receita.id + "')\"><i class='icon-eye'></a></td>");
                $("<tr/>", {
                    html: items.join("")
                }).appendTo("#tab_receitas tbody");
                items = [];
            });
        });
    });
}

function showReceitaDetail(id) {

	$("#receita_detalhes_texto").html("")

    var receita = receitas[id];

    var items = [];

    $.each(receita, function(key, val) {
        
        items.push("<tr><td><strong style='text-transform: uppercase;'>" + key + "</strong></td><td>" + val + "</td></tr>");
    });

    $("<table>", {
    	class:"table table-responsive-sm table-hover table-outline mb-0",
        html: items.join("")
    }).appendTo("#receita_detalhes_texto");


    $("#receitaDetail").modal();
}

function clear() {
    $("#tab_receitas tbody").html("");
    $("#medicamento").val("");
    $("#classe").val("");
    $("#paciente_cpf").val("");
    $("#data_inicio").val("");
    $("#data_fim").val("");
    $("#id_receita").val("");
}