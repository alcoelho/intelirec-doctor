medicamentoAutoComplete("#medicamento_medicamento", "#medicamento_cod");

var medicamentos = [];
var receita = {};
var indexEdit = null;

loadForm();

function loadForm() {
    var data = new Date();
    var dia = data.getDate();
    var mes = data.getMonth();
    var ano = data.getFullYear();
    var str_data = dia + '/' + (mes + 1) + '/' + ano;
    $("#medico_nome").val(localStorage.username);
    $("#medico_crm").val(localStorage.crm);
    $("#medico_endereco").val(localStorage.address);
    $("#data").val(str_data);
}


function auxListMedicamentos() {

    $("#tab_medicamentos tbody").remove();

    var itens = [];

    for (i = 0; i < medicamentos.length; i++) {
        itens.push("<tr>");
        itens.push("<td><span>" + medicamentos[i].nome + "</span></td>");
        itens.push("<td><span>" + medicamentos[i].instrucoes + "</span></td>");
        var actions = "<a style=\"margin:0 20px 0 0;\" href=\"javascript:editRemedio('" + i + "')\"><i class='icon-pencil'/></a>";
        actions = actions + "<a style=\"color:red;\" href=\"javascript:removeRemedio('" + i + "')\"><i class='icon-trash'/></a>";
        itens.push("<td>" + actions + "</td>");
        itens.push("</tr>");
    }

    $("<tbody/>", {
        html: itens.join("")
    }).appendTo("#tab_medicamentos");
    itens = [];
}

function auxClearAllFieldsMedi() {

    $("#tab_medicamentos tbody").remove();
    $("#medicamento_medicamento").val("");
    $("#medicamento_instrucoes").val("");
    $("#medicamento_cod").val("");
}

function auxClearAllFieldRec() {

    $("#medico_nome").val("");
    $("#medico_crm").val("");
    $("#medico_endereco").val("");
    $("#paciente_nome").val("");
    $("#paciente_cpf").val("");
    $("#paciente_rg").val("");
    $("#paciente_endereco").val("");

}

function addMedicamento() {

    var medicamento = $("#medicamento_medicamento").val();
    var medicmanento_instrucoes = $("#medicamento_instrucoes").val();
    var medicmanento_cod = $("#medicamento_cod").val();

    if (medicamento == "" || medicmanento_cod == "" || medicmanento_instrucoes == "") {
        var msg = "Campos obrigatórios não informados";
        showMsg(msg, "#error_msg", true, true);
    }

    var remed = {
        nome: medicamento,
        instrucoes: medicmanento_instrucoes,
        cod: medicmanento_cod
    }

    auxClearAllFieldsMedi();

    if (indexEdit != null) {
        medicamentos[indexEdit] = remed;
        $("#medicamento_medicamento").prop("disabled", false);
        $("#btnAddMed").html("Adicionar");
        $("#btnCancel").hide();
        indexEdit = null;

    } else {
        medicamentos.push(remed);
    }

    auxListMedicamentos();

    showMsg("Medicamento adicionado", "#msg_sucess", false, false);
}


function editRemedio(index) {

    indexEdit = index;
    var remed = medicamentos[index];
    $("#medicamento_medicamento").val(remed.nome);
    $("#medicamento_medicamento").prop("disabled", true);
    $("#medicamento_instrucoes").val(remed.instrucoes);
    $("#medicamento_cod").val(remed.cod);
    $("#btnAddMed").html("Salvar");
    $("#btnCancel").show();

}

function removeRemedio(index) {
    medicamentos.splice(index);
    auxListMedicamentos();
}

function saveReceita() {
    event.preventDefault();

    var receita = {};

    receita.medicoNome = $("#medico_nome").val();
    receita.medicoCrm = $("#medico_crm").val();
    receita.medicoEndereco = $("#medico_endereco").val();
    receita.pacienteNome = $("#paciente_nome").val();
    receita.pacienteCpf = $("#paciente_cpf").val();
    receita.pacienteRg = $("#paciente_rg").val();
    receita.pacienteEndereco = $("#paciente_endereco").val();
    receita.medicamentos = [];

    for (i = 0; i < medicamentos.length; i++) {
        receita.medicamentos.push(medicamentos[i]);
    }

    auxClearAllFieldsMedi();
    auxClearAllFieldRec();
    loadForm();

    medicamentos = [];
    receita = {};
    indexEdit = null;

    console.log(receita);

    showMsg("Receita registrada com sucesso", "#msg_sucess_top", false, true);

}