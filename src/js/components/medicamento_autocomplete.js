function medicamentoAutoComplete(input, hidden) {
    var options = {
        url: function(p) {
            var split = p.split(' - ');
            var product = split[0];
            var apresentacao = split.length > 1 ? split[1] : "";
            return "http://mobile-aceite.tcu.gov.br/mapa-da-saude/rest/remedios?produto=" + product + "&apresentacao=" + apresentacao + "&quantidade=30";
        },

        getValue: function(element) {
            return element.produto + " - " + element.apresentacao;
        },

        list: {
            maxNumberOfElements: 30,
            match: {
                enabled: true
            },
            onSelectItemEvent: function() {
                var selectedItemValue = $(input).getSelectedItemData().cod;
                $(hidden).val(selectedItemValue);
            }
            /*,onHideListEvent: function() {
                $(hidden).val("");
            }*/
        }

    };

    $(input).easyAutocomplete(options);

}